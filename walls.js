//create walls

var smallRH = [];
var smallRV = [];

var wallsH = [];
var wallsV = [];

//create small rectangles that are used to show walls
function smallRectH() {
  posX = boardLocation + 35;
  posY = boardLocation + boardSize / 9 - 5;
  size = boardSize / 40;
  for (i = 0; i < 9; i++) {
    smallRH[i] = [];
    for (j = 0; j < 9; j++) {
      smallRH[i].push({
        x: posX,
        y: posY,
        width: size,
        heigth: size,
        vizible: false,
        selected: false,
        imposible: false,
      });
      posX += boardSize / 9;
    }
    posX = boardLocation + 35;
    posY += boardSize / 9;
  }
}
function smallRectV() {
  posX = boardLocation + 75;
  posY = boardLocation + 30;
  size = boardSize / 40;

  for (i = 0; i < 9; i++) {
    smallRV[i] = [];
    for (j = 0; j < 9; j++) {
      smallRV[i].push({
        x: posX,
        y: posY,
        width: size,
        heigth: size,
        vizible: false,
        selected: false,
        imposible: false,
      });
      posX += 82;
    }
    posX = boardLocation + 75;
    posY += boardSize / 9;
  }
}
function showSmallRects() {
  for (let i = 0; i < smallRH.length - 1; i++) {
    for (let j = 0; j < smallRH.length; j++) {
      smallRH[i][j].vizible = true;
    }
  }
  for (let i = 0; i < smallRV.length; i++) {
    for (let j = 0; j < smallRV.length - 1; j++) {
      smallRV[i][j].vizible = true;
    }
  }

  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      bigR[i][j].possible = false;
      bigR[i][j].possible2 = false;
    }
  }
  move1Possible = 3;
}

// creates the walls
function createWallH() {
  posX = boardLocation;
  posY = boardLocation;
  width = boardSize / 4;
  height = boardSize / 50;
  for (i = 0; i < smallRH.length; i++) {
    wallsH[i] = [];
    for (j = 0; j < smallRH.length; j++) {
      wallsH[i].push({
        x: smallRH[i][j].x - 35,
        y: smallRH[i][j].y,
        width: boardSize / 9,
        heigth: boardSize / 50,
        vizible: false,
        shown: false,
      });
    }
  }
}
function createWallV() {
  posX = boardLocation;
  posY = boardLocation;
  width = boardSize / 4;
  height = boardSize / 50;
  for (let i = 0; i < smallRV.length; i++) {
    wallsV[i] = [];
    for (let j = 0; j < smallRV.length; j++) {
      wallsV[i].push({
        x: smallRV[i][j].x,
        y: smallRV[i][j].y - 30,
        width: boardSize / 50,
        heigth: boardSize / 9,
        vizible: false,
        shown: false,
      });
    }
  }
}
function showWalls() {
  wallsH.forEach((wall) => {
    for (let i = 0; i < wallsH.length; i++) {
      if (wall[i].vizible) {
        fill("black");
        rect(wall[i].x, wall[i].y, wall[i].width, wall[i].heigth);
      }
    }
  });
  wallsV.forEach((wall) => {
    for (let i = 0; i < wallsV.length; i++) {
      if (wall[i].vizible) {
        fill("black");
        rect(wall[i].x, wall[i].y, wall[i].width, wall[i].heigth);
      }
    }
  });
}

//functions that check if mouse is over the small rectangles and if the wall can be created
function sellectRectForWallH() {
  //select horizontal small rects
  for (let i = 0; i < smallRH.length - 1; i++) {
    for (let j = 0; j < smallRH.length; j++) {
      if (
        smallRH[i][j].vizible &&
        !wallsH[i][j].vizible &&
        !smallRH[i][j].imposible
      ) {
        fill(0, 255, 0, 100);
        rect(
          smallRH[i][j].x,
          smallRH[i][j].y,
          smallRH[i][j].width,
          smallRH[i][j].heigth
        );
      }
      if (j < smallRH.length - 1) {
        if (
          smallRH[i][j].selected &&
          smallRH[i][j + 1].selected &&
          !wallsH[i][j].shown &&
          !wallsH[i][j + 1].shown
        ) {
          wallsH[i][j].shown = true;
          wallsH[i][j + 1].shown = true;
          wallsH[i][j].vizible = true;
          wallsH[i][j + 1].vizible = true;
          for (let i = 0; i < smallRH.length; i++) {
            for (let j = 0; j < smallRH.length; j++) {
              smallRH[i][j].vizible = false;
            }
          }
          for (let i = 0; i < smallRV.length; i++) {
            for (let j = 0; j < smallRV.length; j++) {
              smallRV[i][j].vizible = false;
            }
          }
          if (!easy) {
            if (playerTurn == 0) {
              wallCounter1--;
              playerTurn = 1;
            } else if (playerTurn == 1) {
              wallCounter2--;
              playerTurn = 0;
            }
          } else if (easy) {
          }
        }
      }
    }
  }
}

function sellectRectForWallV() {
  //select vertical small rects
  for (let i = 0; i < smallRV.length; i++) {
    for (let j = 0; j < smallRV.length - 1; j++) {
      if (
        smallRV[i][j].vizible &&
        !wallsV[i][j].vizible &&
        !smallRV[i][j].imposible
      ) {
        fill(255, 0, 0, 100);
        rect(
          smallRV[i][j].x,
          smallRV[i][j].y,
          smallRV[i][j].width,
          smallRV[i][j].heigth
        );
      }
      if (i < smallRV.length - 1) {
        if (
          smallRV[i][j].selected &&
          smallRV[i + 1][j].selected &&
          !wallsV[i][j].shown &&
          !wallsV[i + 1][j].shown
        ) {
          wallsV[i][j].shown = true;
          wallsV[i + 1][j].shown = true;
          wallsV[i][j].vizible = true;
          wallsV[i + 1][j].vizible = true;

          for (let i = 0; i < smallRH.length; i++) {
            for (let j = 0; j < smallRH.length; j++) {
              smallRH[i][j].vizible = false;
            }
          }
          for (let i = 0; i < smallRV.length; i++) {
            for (let j = 0; j < smallRV.length; j++) {
              smallRV[i][j].vizible = false;
            }
          }
          if (!easy) {
            if (playerTurn == 0) {
              wallCounter1--;
              playerTurn = 1;
            } else if (playerTurn == 1) {
              wallCounter2--;
              playerTurn = 0;
            }
          } else if (easy) {
          }
        }
      }
    }
  }
}

function selectWalls() {
  for (let j = 0; j < smallRH.length; j++) {
    for (let i = 0; i < smallRH.length; i++) {
      if (
        mouseIsPressed &&
        mouseX > smallRH[i][j].x &&
        mouseX < smallRH[i][j].x + smallRH[i][j].width &&
        mouseY > smallRH[i][j].y &&
        mouseY < smallRH[i][j].y + smallRH[i][j].heigth &&
        smallRH[i][j].vizible &&
        !smallRH[i][j].imposible
      ) {
        smallRH[i][j].selected = true;
        smallRH[i][j].vizible = false;
      }
    }
  }

  smallRV.forEach((rects, index) => {
    for (let i = 0; i < smallRV.length; i++) {
      if (
        mouseIsPressed &&
        mouseX > rects[i].x &&
        mouseX < rects[i].x + rects[i].width &&
        mouseY > rects[i].y &&
        mouseY < rects[i].y + rects[i].heigth &&
        rects[i].vizible &&
        !rects[i].imposible
      ) {
        rects[i].vizible = false;
        rects[i].selected = true;
      }
    }
  });
}

//checks where players can move to
function checkWallsPlayer1() {
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      if (player1[i][j].vizible) {
        if (wallsH[i][j].vizible) bigR[i + 1][j].possible = false;
        if (wallsV[i][j].vizible) bigR[i][j + 1].possible = false;
        if (j > 0)
          if (wallsV[i][j - 1].vizible) bigR[i][j - 1].possible = false;
        if (i > 0) {
          if (wallsH[i - 1][j].vizible) bigR[i - 1][j].possible = false;
        }
      }
    }
  }
}
function checkWallsPlayer2() {
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      if (player2[i][j].vizible) {
        if (i > 0)
          if (wallsH[i - 1][j].vizible) bigR[i - 1][j].possible2 = false;
        if (j > 0)
          if (wallsV[i][j - 1].vizible) bigR[i][j - 1].possible2 = false;
        if (wallsV[i][j].vizible) bigR[i][j + 1].possible2 = false;
        if (i < 8) {
          if (wallsH[i][j].vizible) bigR[i + 1][j].possible2 = false;
        }
      }
    }
  }
}
function checkAgainstPlayer1() {
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      if (player2[i][j].vizible) {
        if (i > 0) {
          //check below
          if (player1[i - 1][j].vizible) {
            bigR[i - 1][j].possible2 = false;
            if (i > 2)
              if (move1Possible == 0 && !wallsH[i - 2][j].vizible) {
                bigR[i - 2][j].possible2 = true;
              }
            if (
              i > 1 &&
              j < 8 &&
              move1Possible == 0 &&
              wallsH[i - 2][j].vizible &&
              !wallsV[i - 1][j].vizible
            ) {
              bigR[i - 1][j + 1].possible2 = true;
            }
            if (
              i > 1 &&
              j < 8 &&
              j > 1 &&
              move1Possible == 0 &&
              wallsH[i - 2][j].vizible &&
              !wallsV[i - 1][j + 1].vizible
            ) {
              bigR[i - 1][j - 1].possible2 = true;
            }

            if (
              i > 2 &&
              j < 8 &&
              j > 1 &&
              move1Possible == 0 &&
              wallsH[i - 2][j].vizible &&
              !wallsV[i - 1][j + 1].vizible
            ) {
              bigR[i - 1][j - 1].possible2 = true;
            }

            if (
              i > 2 &&
              j > 7 &&
              move1Possible == 0 &&
              wallsH[i - 2][j].vizible
            ) {
              bigR[i - 1][j - 1].possible2 = true;
            }
          } else if (i < 8 && player1[i + 1][j].vizible) {
            //check above
            bigR[i + 1][j].possible2 = false;
            if (move1Possible == 0 && !wallsH[i + 1][j].vizible) {
              bigR[i + 2][j].possible2 = true;
            }
            if (
              move1Possible == 0 &&
              wallsH[i + 1][j].vizible &&
              !wallsV[i + 1][j].vizible
            ) {
              bigR[i + 1][j + 1].possible2 = true;
            }
            if (
              move1Possible == 0 &&
              wallsH[i + 1][j].vizible &&
              !wallsV[i + 1][j - 1].vizible
            ) {
              bigR[i + 1][j - 1].possible2 = true;
            }
          }
        }

        //check right
        if (j < 8) {
          if (player1[i][j + 1].vizible) {
            bigR[i][j + 1].possible2 = false;
            if (j < 7)
              if (move1Possible == 0 && !wallsV[i][j + 1].vizible) {
                bigR[i][j + 2].possible2 = true;
              }
            if (
              move1Possible == 0 &&
              wallsV[i][j + 1].vizible &&
              !wallsH[i - 1][j + 1].vizible
            ) {
              bigR[i - 1][j + 1].possible2 = true;
            }
            if (
              move1Possible == 0 &&
              wallsV[i][j + 1].vizible &&
              !wallsH[i][j + 1].vizible
            ) {
              bigR[i + 1][j + 1].possible2 = true;
            }
          }
        }

        //check left
        if (j > 0) {
          if (player1[i][j - 1].vizible) {
            bigR[i][j - 1].possible2 = false;
            if (move1Possible == 0 && !wallsV[i][j - 2].vizible) {
              bigR[i][j - 2].possible2 = true;
            }
            if (
              move1Possible == 0 &&
              wallsV[i][j - 2].vizible &&
              !wallsH[i - 1][j - 1].vizible
            ) {
              bigR[i - 1][j - 1].possible2 = true;
            }
            if (
              move1Possible == 0 &&
              wallsV[i][j - 2].vizible &&
              !wallsH[i][j - 1].vizible
            ) {
              bigR[i + 1][j - 1].possible2 = true;
            }
          }
        }
      }
    }
  }
}
function checkAgainstPlayer2() {
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      if (player1[i][j].vizible) {
        //check above
        if (i < 8) {
          if (player2[i + 1][j].vizible) {
            bigR[i + 1][j].possible = false;
            if (i < 7)
              if (move1Possible == 1 && !wallsH[i + 1][j].vizible) {
                bigR[i + 2][j].possible = true;
              }
            if (
              j < 8 &&
              move1Possible == 1 &&
              wallsH[i + 1][j].vizible &&
              !wallsV[i + 1][j].vizible
            ) {
              bigR[i + 1][j + 1].possible = true;
            }
            if (
              j > 1 &&
              move1Possible == 1 &&
              wallsH[i + 1][j].vizible &&
              !wallsV[i + 1][j - 1].vizible
            ) {
              bigR[i + 1][j - 1].possible = true;
            }
          }
        }
        //check below
        if (i > 0) {
          if (player2[i - 1][j].vizible) {
            bigR[i - 1][j].possible = false;
            if (move1Possible == 1 && !wallsH[i - 2][j].vizible) {
              bigR[i - 2][j].possible = true;
            }
            if (
              move1Possible == 1 &&
              wallsH[i - 2][j].vizible &&
              !wallsV[i - 1][j].vizible
            ) {
              bigR[i - 1][j - 1].possible = true;
            }
            if (
              move1Possible == 1 &&
              wallsH[i - 2][j].vizible &&
              !wallsV[i - 1][j + 1].vizible
            ) {
              bigR[i - 1][j - 1].possible = true;
            }
          }
        }
        //check to the right
        if (j < 8) {
          if (player2[i][j + 1].vizible) {
            bigR[i][j + 1].possible = false;
            if (move1Possible == 1 && !wallsV[i][j + 1].vizible) {
              bigR[i][j + 2].possible = true;
            }
            if (
              move1Possible == 1 &&
              wallsV[i][j + 1].vizible &&
              !wallsH[i - 1][j + 1].vizible
            ) {
              bigR[i - 1][j + 1].possible = true;
            }
            if (
              move1Possible == 1 &&
              wallsV[i][j + 1].vizible &&
              !wallsH[i][j + 1].vizible
            ) {
              bigR[i + 1][j + 1].possible = true;
            }
          }
        }
        //check to the left
        if (j > 0) {
          if (player2[i][j - 1].vizible) {
            bigR[i][j - 1].possible = false;
            if (move1Possible == 1 && !wallsV[i][j - 2].vizible) {
              bigR[i][j - 2].possible = true;
            }
            if (
              move1Possible == 0 &&
              wallsV[i][j - 2].vizible &&
              !wallsH[i - 1][j - 1].vizible
            ) {
              bigR[i - 1][j - 1].possible2 = true;
            }
            if (
              move1Possible == 0 &&
              wallsV[i][j - 2].vizible &&
              !wallsH[i][j - 1].vizible
            ) {
              bigR[i + 1][j - 1].possible2 = true;
            }
          }
        }
      }
    }
  }
}
function stopClosingWalls() {
  for (i = 0; i < smallRH.length; i++) {
    for (j = 0; j < smallRH.length; j++) {
      if (i < 8 && j < 8) {
        if (wallsV[i][j].vizible && wallsV[i + 1][j].vizible) {
          smallRH[i][j].imposible = true;
          smallRH[i][j + 1].imposible = true;
          if (j < 7) {
            if (smallRH[i][j + 2].selected) {
              smallRH[i][j + 1].imposible = false;
            }
            if (wallsV[i][j + 2].vizible) {
              smallRH[i][j + 1].imposible = false;
            }
          }
          if (j > 1) {
            if (smallRH[i][j - 1].selected) {
              smallRH[i][j].imposible = false;
            }
          }
        }
      }
      if (i < 8 && j < 8) {
        if (wallsH[i][j].vizible && wallsH[i][j + 1].vizible) {
          smallRV[i][j].imposible = true;
          smallRV[i + 1][j].imposible = true;
          if (i < 7) {
            if (smallRV[i + 2][j].selected) {
              smallRV[i + 1][j].imposible = false;
            }
            if (wallsH[i + 2][j].vizible) {
              smallRV[i + 1][j].imposible = false;
            }
            if (wallsH[i + 1][j].vizible) {
              smallRV[i][j].imposible = true;
            }
          }
          if (i > 1) {
            if (smallRV[i - 1][j].selected) {
              smallRV[i][j].imposible = false;
            }
          }
        }
      }
    }
  }
}
