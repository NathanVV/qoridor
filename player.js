var start = 0;

var move1Possible = 3;
var player1 = [];
var player2 = [];
var playerTurn = 3;
var wallCounter1 = 8;
var wallCounter2 = 8;

var player1Name;
var player2Name;

//select movement type
function move() {
  for (let i = 0; i < smallRH.length; i++) {
    for (let j = 0; j < smallRH.length; j++) {
      smallRH[i][j].vizible = false;
    }
  }
  for (let i = 0; i < smallRV.length; i++) {
    for (let j = 0; j < smallRV.length; j++) {
      smallRV[i][j].vizible = false;
    }
  }
  move1Possible = 1;
}
function move2() {
  for (let i = 0; i < smallRH.length; i++) {
    for (let j = 0; j < smallRH.length; j++) {
      smallRH[i][j].vizible = false;
    }
  }
  for (let i = 0; i < smallRV.length; i++) {
    for (let j = 0; j < smallRV.length; j++) {
      smallRV[i][j].vizible = false;
    }
  }
  move1Possible = 0;
}

//create players
function character1() {
  posX = 50;
  posY = 50;
  size = boardSize / 9;

  player1 = [];
  for (i = 0; i < 9; i++) {
    player1[i] = [];
    for (j = 0; j < 9; j++) {
      player1[i].push({
        x: posX,
        y: posY,
        width: size / 2,
        heigth: size / 2,
        color: "red",
        vizible: false,
      });
      posX += boardSize / 9;
    }
    posX = 50;
    posY += boardSize / 9;
  }
}
function character2() {
  posX = 50;
  posY = 50;
  size = boardSize / 9;

  player2 = [];
  for (i = 0; i < 9; i++) {
    player2[i] = [];
    for (j = 0; j < 9; j++) {
      player2[i].push({
        x: posX,
        y: posY,
        width: size / 2,
        heigth: size / 2,
        color: "pink",
        vizible: false,
      });
      posX += boardSize / 9;
    }
    posX = 50;
    posY += boardSize / 9;
  }
}
function showCharacter() {
  player1.forEach((sRect, index) => {
    sRect.forEach((element) => {
      if (element.vizible) {
        fill(element.color);
        rect(element.x, element.y, element.width, element.heigth);
        element.nr++;
      }
    });
  });

  player2.forEach((sRect, index) => {
    sRect.forEach((element) => {
      if (element.vizible) {
        fill(element.color);
        rect(element.x, element.y, element.width, element.heigth);
        element.nr++;
      }
    });
  });
}

//allows player to move is move is possible
function MovePlayer1() {
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      if (
        bigR[i][j].possible &&
        mouseIsPressed &&
        mouseX > bigR[i][j].x &&
        mouseX < bigR[i][j].x + bigR[i][j].width &&
        mouseY > bigR[i][j].y &&
        mouseY < bigR[i][j].y + bigR[i][j].heigth
      ) {
        for (k = 0; k < bigR.length; k++) {
          for (m = 0; m < bigR.length; m++) {
            player1[k][m].vizible = false;
          }
        }
        move1Possible = 3;
        player1[i][j].vizible = true;
        playerTurn = 1;
      }
    }
  }
}
function MovePlayer2() {
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      if (
        bigR[i][j].possible2 &&
        mouseIsPressed &&
        mouseX > bigR[i][j].x &&
        mouseX < bigR[i][j].x + bigR[i][j].width &&
        mouseY > bigR[i][j].y &&
        mouseY < bigR[i][j].y + bigR[i][j].heigth
      ) {
        for (k = 0; k < bigR.length; k++) {
          for (m = 0; m < bigR.length; m++) {
            player2[k][m].vizible = false;
          }
        }
        move1Possible = 3;
        player2[i][j].vizible = true;
        playerTurn = 0;
      }
    }
  }
}
