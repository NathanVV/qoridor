function wallCounters() {
  wallCounter1Button = createButton(
    player1Name + " has: " + wallCounter1 + " walls"
  );
  wallCounter1Button.style("background-color", "red");
  wallCounter1Button.position(800, 200);
  wallCounter1Button.size(100, 40);
  wallCounter2Button = createButton(
    player2Name + " has: " + wallCounter2 + " walls"
  );
  wallCounter2Button.style("background-color", "pink");
  wallCounter2Button.size(100, 40);
  wallCounter2Button.position(800, 600);
}

function gameplay() {
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      player2[i][j].vizible = false;
    }
  }
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      player1[i][j].vizible = false;
    }
  }
  player1[0][4].vizible = true;
  player2[8][4].vizible = true;
  for (i = 0; i < bigR.length; i++) {
    for (j = 0; j < bigR.length; j++) {
      bigR[i][j].possible = false;
      bigR[i][j].possible2 = false;
      bigR[i][j].vizible = true;
    }
  }
  smallRH.forEach((sRect, index) => {
    sRect.vizible = false;
    sRect.selected = false;
  });
  smallRV.forEach((sRect, index) => {
    sRect.vizible = false;
    sRect.selected = false;
  });
  wallsH.forEach((wall, index) => {
    wall.vizible = false;
    wall.shown = false;
  });
  wallsV.forEach((wall, index) => {
    wall.vizible = false;
    wall.shown = false;
  });
  bigR.forEach((element) => {
    element.possible = false;
  });
  start = 1;
  playerTurn = 0;

  player1Name = inp.value();

  movePlayer1 = createButton(player1Name + " move");
  movePlayer1.style("background-color", "red");
  movePlayer1.position(800, 380);
  movePlayer1.size(100, 50);
  movePlayer1.mousePressed(move);

  buildWall1 = createButton(player1Name + " wall");
  buildWall1.style("background-color", "red");
  buildWall1.position(800, 440);
  buildWall1.size(100, 50);
  buildWall1.mousePressed(showSmallRects);

  player2Name = inp2.value();
  fill("pink");
  movePlayer2 = createButton(player2Name + " move");
  movePlayer2.style("background-color", "pink");
  movePlayer2.position(800, 380);
  movePlayer2.size(100, 50);
  movePlayer2.mousePressed(move2);
  movePlayer2.hide();

  buildWall2 = createButton(player2Name + " wall");
  buildWall2.style("background-color", "pink");
  buildWall2.position(800, 440);
  buildWall2.size(100, 50);
  buildWall2.mousePressed(showSmallRects);
  buildWall2.hide();

  wallCounters();
}

function restart() {
  window.location.reload();
}

function beginningButtons() {
  restartButton = createButton("Restart");
  restartButton.position(750, 790);
  restartButton.mousePressed(restart);

  PvPstartButton = createButton("PvP");
  PvPstartButton.position(150, 350);
  PvPstartButton.mousePressed(gameplay);
  PvPstartButton.size(100, 100);

  PvEstartButton = createButton("PvE");
  PvEstartButton.position(550, 350);
  PvEstartButton.mousePressed(chooseDificulty);
  PvEstartButton.size(100, 100);
}

function chooseDificulty() {
  start = 3;
  PvEEstartButton = createButton("Easy");
  PvEEstartButton.position(150, 350);
  PvEEstartButton.mousePressed(easyMode);
  PvEEstartButton.size(100, 100);

  PvEMstartButton = createButton("Medium");
  PvEMstartButton.position(550, 350);
  PvEMstartButton.mousePressed(mediumMode);
  PvEMstartButton.size(100, 100);
}

function easyMode() {
  PvEEstartButton.hide();
  PvEMstartButton.hide();
  start = 1;
  easy = true;
  gameplay();
}

function mediumMode() {
  PvEEstartButton.hide();
  PvEMstartButton.hide();

  medium = true;
  gameplay();
}
