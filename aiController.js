var leftRight = false;
var goUp = true;

function randomNR() {
  moveOrPlace = Math.floor(random(0, 2));
  nr = Math.floor(random(0, 4));
  if (temp == 0) {
    r = Math.floor(random(0, 7));
    r2 = Math.floor(random(0, 6));
    choose = Math.floor(random(0, 2));

    temp++;
  }
}

function basicAiMovement() {
  randomNR();
  move2();
  for (let i = 0; i < bigR.length; i++) {
    for (let j = 0; j < bigR.length; j++) {
      if (player2[i][j].vizible) {
        if (nr == 0 && i > 0 && bigR[i - 1][j].possible2 == true) {
          player2[i - 1][j].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        } else if (nr == 0 && i > 2 && bigR[i - 2][j].possible2 == true) {
          player2[i - 2][j].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        } else if (nr == 1 && j > 1 && bigR[i][j - 1].possible2 == true) {
          player2[i][j - 1].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        } else if (nr == 2 && j < 8 && bigR[i][j + 1].possible2 == true) {
          player2[i][j + 1].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        } else if (nr == 3 && i < 8 && bigR[i + 1][j].possible2 == true) {
          player2[i + 1][j].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        }
      }
    }
  }
}

function basicAiWall() {
  randomNR();

  if (choose == 1) {
    if (!smallRH[r][r2].imposible && !smallRH[r][r2 + 1].imposible) {
      smallRH[r][r2].vizible = false;
      smallRH[r][r2].selected = true;
      smallRH[r][r2 + 1].vizible = false;
      smallRH[r][r2 + 1].selected = true;
      wallCounter2--;
      playerTurn = 0;
    }
  } else {
    if (!smallRV[r][r2].imposible && !smallRV[r + 1][r2].imposible) {
      smallRV[r][r2].vizible = false;
      smallRV[r][r2].selected = true;
      smallRV[r + 1][r2].vizible = false;
      smallRV[r + 1][r2].selected = true;
      wallCounter2--;
      playerTurn = 0;
    }
  }
}

function mediumAiMovement() {
  move2();
  aiVsWalls();
  for (let i = 0; i < bigR.length; i++) {
    for (let j = 0; j < bigR.length; j++) {
      if (player2[i][j].vizible) {
        if (goUp && i > 0 && bigR[i - 1][j].possible2 == true) {
          player2[i - 1][j].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        } else if (goUp && i > 2 && bigR[i - 2][j].possible2 == true) {
          player2[i - 2][j].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        } else if (!leftRight && j > 0 && bigR[i][j - 1].possible2 == true) {
          player2[i][j - 1].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
          goUp = true;
        } else if (leftRight && j < 8 && bigR[i][j + 1].possible2 == true) {
          player2[i][j + 1].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
          goUp = true;
        } else if (!goUp && i < 8 && bigR[i + 1][j].possible2 == true) {
          player2[i + 1][j].vizible = true;
          player2[i][j].vizible = false;
          playerTurn = 0;
          move1Possible = 3;
        }
      }
    }
  }
}

function aiVsWalls() {
  var countLeftFromP = 0;
  var countRightFromP = 0;
  let iPos;
  let jPos;
  for (let i = 0; i < bigR.length; i++) {
    for (let j = 0; j < bigR.length; j++) {
      if (player2[i][j].vizible) {
        iPos = i;
        jPos = j;
      }
    }
  }

  for (let i = jPos; i >= 0; i--) {
    if (wallsH[iPos - 1][i].vizible) {
      countLeftFromP++;
    }
  }

  for (let j = jPos; j <= 8; j++) {
    if (wallsH[iPos - 1][j].vizible) {
      countRightFromP++;
    }
  }

  if (
    wallsH[iPos - 1][jPos].vizible &&
    wallsV[iPos][jPos - 1].vizible &&
    wallsV[iPos][jPos].vizible
  ) {
    goUp = false;
  }
  if (countLeftFromP > countRightFromP) {
    leftRight = true;
  } else {
    leftRight = false;
  }
  if (countLeftFromP == jPos + 1) {
    leftRight = true;
  }
  if (countRightFromP == 9 - jPos) {
    leftRight = false;
  }

  var countUpDownR = 0;
  var countUpDownL = 0;

  for (let i = iPos; i <= 8; i++) {
    if (wallsV[i][jPos].vizible) {
      countUpDownR++;
    }
  }
  for (let j = iPos; j <= 8; j++) {
    if (wallsV[j][jPos - 1].vizible) {
      countUpDownL++;
    }
  }
  if (countUpDownL >= countLeftFromP) {
    leftRight = true;
  }
  if (countUpDownR >= countRightFromP) {
    leftRight = false;
  }

  if (jPos > 0)
    if (wallsV[iPos][jPos - 1].vizible && wallsV[iPos][jPos + 1].vizible) {
      leftRight = false;
      goUp = false;
    }
  if (jPos > 1)
    if (wallsV[iPos][jPos - 2].vizible && wallsV[iPos][jPos].vizible) {
      leftRight = true;
      goUp = false;
    }
}

function memdiumAiWall() {
  var minus = false;
  for (let i = 0; i < bigR.length; i++) {
    for (let j = 0; j < bigR.length; j++) {
      if (player1[i][j].vizible) {
        if (j < 8)
          if (!wallsH[i][j].vizible && !wallsH[i][j + 1].vizible) {
            smallRH[i][j].vizible = false;
            smallRH[i][j].selected = true;
            smallRH[i][j + 1].vizible = false;
            smallRH[i][j + 1].selected = true;
            playerTurn = 0;
            minus = true;
          }
      }
    }
  }
  if (minus) {
    wallCounter2--;
    minus = false;
  }
}
