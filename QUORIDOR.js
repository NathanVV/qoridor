var value;
var moveX, moveY;
var rectLength, rectHight;
var rectLengthTemp, rectHightTemp;
var posX;
var posY;
var size;
var bigR = [];
var moveOrPlace = 1;
var nr = Math.floor(Math.random(0, 4));

let r = Math.floor(Math.random(0, 8));
let r2 = Math.floor(Math.random(0, 2));
var aiCreateWall = false;

var temp = 0;

let choose = 1;
var chooseAction = true;

var boardLocation = 30;
var canvasSize = 800,
  boardSize = canvasSize - boardLocation * 2;

var easy = false;
var medium = false;
function setup() {
  createCanvas(canvasSize, canvasSize);
  value = 0;
  rectHight = 25;
  rectLength = 25;
  rectLengthTemp = rectLength;
  rectHightTemp = rectHight;
  posX = 10;
  posY = 10;
  size = 25;

  //create game table
  smallRectH();
  smallRectV();
  bigRect();
  createWallH();
  createWallV();
  character1();
  character2();

  //add names
  inp = createInput("Player 1");
  inp.position(35, 15);
  inp.size(70);

  inp2 = createInput("Player 2");
  inp2.position(700, 15);
  inp2.size(70);

  //restart and start buttons
  beginningButtons();
}
function draw() {
  background(220);

  fill("white");
  rect(boardLocation, boardLocation, boardSize, boardSize);

  //game table

  showBigRect();
  sellectRectForWallH();
  sellectRectForWallV();
  stopClosingWalls();
  showWalls();
  showCharacter();

  //movement
  MovePlayer1();
  MovePlayer2();
  movementScript();

  selectWalls();

  //check if wall is possible
  checkWallsPlayer1();
  checkWallsPlayer2();
  checkAgainstPlayer1();
  checkAgainstPlayer2();

  //end game
  playerWin();
  // hide and show start and restart button
  if (start == 1) {
    PvPstartButton.hide();
    PvEstartButton.hide();
    wallCounters();
    inp.position(35, 15);
    inp2.position(700, 15);
    inp.show();
    inp2.show();
  } else if (start == 0) {
    inp.position(200, 200);
    inp2.position(500, 200);
    PvPstartButton.show();
  } else if (start == 3) {
    inp.position(200, 200);
    inp2.position(500, 200);
    inp.hide();
    inp2.hide();
    PvPstartButton.hide();
    PvEstartButton.hide();
  }

  //hide and show buttons
  if (playerTurn == 1) {
    movePlayer1.hide();
    buildWall1.hide();
    movePlayer2.show();
    if (wallCounter2 > 0) buildWall2.show();
  } else if (playerTurn == 0) {
    movePlayer1.show();
    if (wallCounter1 > 0) buildWall1.show();
    movePlayer2.hide();
    buildWall2.hide();
  }

  if (easy == true) {
    inp2.value("BOT");
    player2Name = "BOT";
    //ai controller
    if (playerTurn == 1) {
      if (moveOrPlace == 1) {
        basicAiMovement();
      } else if (moveOrPlace != 1 && wallCounter2 > 0) {
        basicAiWall();
        temp = 0;
      } else {
        basicAiMovement();
      }
    }
  }

  if (medium == true) {
    inp2.value("BOT");
    player2Name = "BOT";
    //ai controller
    if (playerTurn == 1) {
      let iPos;
      let jPos;
      for (let i = 0; i < bigR.length; i++) {
        for (let j = 0; j < bigR.length; j++) {
          if (player1[i][j].vizible) {
            iPos = i;
            jPos = j;
          }
        }
      }
      if (
        iPos > 3 &&
        wallCounter2 > 0 &&
        !wallsH[iPos][jPos].vizible &&
        !wallsH[iPos][jPos + 1].vizible
      ) {
        memdiumAiWall();
      } else {
        mediumAiMovement();
      }
    }
  }
}

function movementScript() {
  // 1 means first player movement
  // 0 means second player movement
  // 3 means no movement
  if (move1Possible == 1) {
    for (i = 0; i < bigR.length; i++) {
      for (j = 0; j < bigR.length; j++) {
        bigR[i][j].possible2 = false;
        if (j < 1) {
          if (i < 1) {
            if (player1[i][j].vizible) {
              bigR[i][j + 1].possible = true;
              bigR[i + 1][j].possible = true;
            }
          } else if (i > 7) {
            if (player1[i][j].vizible) {
              bigR[i][j + 1].possible = true;
              bigR[i - 1][j].possible = true;
            }
          } else if (i > 0 && i < 8) {
            if (player1[i][j].vizible) {
              bigR[i][j + 1].possible = true;
              bigR[i + 1][j].possible = true;
              bigR[i - 1][j].possible = true;
            }
          }
        } else if (j > 7) {
          if (i < 1) {
            if (player1[i][j].vizible) {
              bigR[i][j - 1].possible = true;
              bigR[i + 1][j].possible = true;
            }
          } else if (i > 7) {
            if (player1[i][j].vizible) {
              bigR[i][j - 1].possible = true;
              bigR[i - 1][j].possible = true;
            }
          } else if (i > 0 && i < 8) {
            if (player1[i][j].vizible) {
              bigR[i][j - 1].possible = true;
              bigR[i + 1][j].possible = true;
              bigR[i - 1][j].possible = true;
            }
          }
        } else if (j > 0 && j < 8) {
          if (i < 1) {
            if (player1[i][j].vizible) {
              bigR[i][j - 1].possible = true;
              bigR[i][j + 1].possible = true;
              bigR[i + 1][j].possible = true;
            }
          } else if (i > 7) {
            if (player1[i][j].vizible) {
              bigR[i][j - 1].possible = true;
              bigR[i][j + 1].possible = true;
              bigR[i - 1][j].possible = true;
            }
          } else if (i > 0 && i < 8) {
            if (player1[i][j].vizible) {
              bigR[i][j - 1].possible = true;
              bigR[i][j + 1].possible = true;
              bigR[i + 1][j].possible = true;
              bigR[i - 1][j].possible = true;
            }
          }
        }
      }
    }
  }
  if (move1Possible == 0) {
    for (i = 0; i < bigR.length; i++) {
      for (j = 0; j < bigR.length; j++) {
        if (j < 1) {
          if (i < 1) {
            if (player2[i][j].vizible) {
              bigR[i][j + 1].possible2 = true;
              bigR[i + 1][j].possible2 = true;
            }
          } else if (i > 7) {
            if (player2[i][j].vizible) {
              bigR[i][j + 1].possible2 = true;
              bigR[i - 1][j].possible2 = true;
            }
          } else if (i > 0 && i < 8) {
            if (player2[i][j].vizible) {
              bigR[i][j + 1].possible2 = true;
              bigR[i + 1][j].possible2 = true;
              bigR[i - 1][j].possible2 = true;
            }
          }
        } else if (j > 7) {
          if (i < 1) {
            if (player2[i][j].vizible) {
              bigR[i][j - 1].possible2 = true;
              bigR[i + 1][j].possible2 = true;
            }
          } else if (i > 7) {
            if (player2[i][j].vizible) {
              bigR[i][j - 1].possible2 = true;
              bigR[i - 1][j].possible2 = true;
            }
          } else if (i > 0 && i < 8) {
            if (player2[i][j].vizible) {
              bigR[i][j - 1].possible2 = true;
              bigR[i + 1][j].possible2 = true;
              bigR[i - 1][j].possible2 = true;
            }
          }
        } else if (j > 0 && j < 8) {
          if (i < 1) {
            if (player2[i][j].vizible) {
              bigR[i][j - 1].possible2 = true;
              bigR[i][j + 1].possible2 = true;
              bigR[i + 1][j].possible2 = true;
            }
          } else if (i > 7) {
            if (player2[i][j].vizible) {
              bigR[i][j - 1].possible2 = true;
              bigR[i][j + 1].possible2 = true;
              bigR[i - 1][j].possible2 = true;
            }
          } else if (i > 0 && i < 8) {
            if (player2[i][j].vizible) {
              bigR[i][j - 1].possible2 = true;
              bigR[i][j + 1].possible2 = true;
              bigR[i + 1][j].possible2 = true;
              bigR[i - 1][j].possible2 = true;
            }
          }
        }
      }
    }
  }
  if (move1Possible == 3) {
    for (i = 0; i < bigR.length; i++) {
      for (j = 0; j < bigR.length; j++) {
        bigR[i][j].possible = false;
        bigR[i][j].possible2 = false;
      }
    }
  }
}

//ractangles that make up the game board
function bigRect() {
  posX = 30;
  posY = 30;
  size = boardSize / 9;

  for (i = 0; i < 9; i++) {
    bigR[i] = [];

    for (j = 0; j < 9; j++) {
      bigR[i].push({
        x: posX,
        y: posY,
        width: size,
        heigth: size,
        color: "orange",
        vizible: false,
        possible: false,
        possible2: false,
      });
      posX += boardSize / 9;
    }
    posX = 30;
    posY += boardSize / 9;
  }
}

function showBigRect() {
  bigR.forEach((bRect) => {
    bRect.forEach((element) => {
      if (element.vizible) {
        fill(element.color);
        rect(element.x, element.y, element.width, element.heigth);
      }
      if (element.possible) {
        element.color = "lightgreen";
      } else if (element.possible2) {
        element.color = "yellow";
      } else {
        element.color = "orange";
      }
    });
  });
}

function playerWin() {
  for (let i = 0; i < bigR.length; i++) {
    for (let j = 0; j < bigR.length; j++) {
      if (player1[8][j].vizible) {
        clear();
        fill("white");
        rect(boardLocation, boardLocation, boardSize, boardSize);
        fill("black");
        textSize(50);
        text(player1Name + " has won the game.", 100, 400);
      }
      if (player2[0][j].vizible) {
        clear();
        fill("white");
        rect(boardLocation, boardLocation, boardSize, boardSize);
        fill("black");
        textSize(50);
        text(player2Name + " has won the game.", 100, 400);
      }
    }
  }
}
